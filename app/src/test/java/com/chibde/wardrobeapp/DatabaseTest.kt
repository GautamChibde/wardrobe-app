package com.chibde.wardrobeapp

import android.os.Build.VERSION_CODES.LOLLIPOP
import com.chibde.wardrobeapp.db.DatabaseHelper
import com.chibde.wardrobeapp.model.Favourite
import com.chibde.wardrobeapp.model.Garment
import com.j256.ormlite.table.TableUtils
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config


/**
 * Created by gautam on 25/6/18.
 */
@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class,
        manifest = Config.NONE,
        sdk = [LOLLIPOP],
        packageName = "com.chibde.wardrobeapp")
class DatabaseTest {
    private lateinit var dbHelper: DatabaseHelper
    private val dbModelHelper = DBModelHelper()

    @Before
    fun setup() {
        dbHelper = DatabaseHelper(RuntimeEnvironment.application)
    }

    @Test
    fun testGarmentDao() {
        // Setup
        TableUtils.clearTable(dbHelper.connectionSource, Garment::class.java)
        // Then
        assertEquals(dbHelper.getGarmentDao().create(dbModelHelper.testGarment1), 1)
        assertEquals(dbHelper.getGarmentDao().create(dbModelHelper.testGarment2), 1)

        // Then
        assertEquals(dbHelper.getGarmentDao().delete(dbModelHelper.testGarment1), 1)
        assertEquals(dbHelper.getGarmentDao().delete(dbModelHelper.testGarment2), 1)
    }

    @Test
    fun testFavouriteDao() {
        // Setup
        TableUtils.clearTable(dbHelper.connectionSource, Garment::class.java)
        TableUtils.clearTable(dbHelper.connectionSource, Favourite::class.java)

        // Given
        assertEquals(dbHelper.getGarmentDao().create(dbModelHelper.testGarment1), 1)
        assertEquals(dbHelper.getGarmentDao().create(dbModelHelper.testGarment2), 1)
        val favourite = Favourite(pant = dbModelHelper.testGarment1,
                shirt = dbModelHelper.testGarment2)

        // Then
        assertEquals(dbHelper.getFavouriteDao().create(favourite), 1)

        // Then
        assertEquals(dbHelper.getFavouriteDao().delete(favourite), 1)
    }
}