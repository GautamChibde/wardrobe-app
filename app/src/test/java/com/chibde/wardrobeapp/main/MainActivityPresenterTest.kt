package com.chibde.wardrobeapp.main

import android.os.Build
import com.chibde.wardrobeapp.BuildConfig
import com.chibde.wardrobeapp.DBModelHelper
import com.chibde.wardrobeapp.db.DatabaseHelper
import com.chibde.wardrobeapp.main.interactor.GetGarmentsInteractor
import com.chibde.wardrobeapp.model.Garment
import com.j256.ormlite.table.TableUtils
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config

/**
 * Created by gautam on 28/6/18.
 */
@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class,
        manifest = Config.NONE,
        sdk = [(Build.VERSION_CODES.LOLLIPOP)],
        packageName = "com.chibde.wardrobeapp")
class MainActivityPresenterTest {
    private lateinit var dbHelper: DatabaseHelper
    private val dbModelHelper = DBModelHelper()

    @Before
    fun setup() {
        dbHelper = DatabaseHelper(RuntimeEnvironment.application)
    }

    @Test
    fun testExm() {
        // Setup
        TableUtils.clearTable(dbHelper.connectionSource, Garment::class.java)
        // Then
        assertEquals(dbHelper.getGarmentDao().create(dbModelHelper.testGarment1), 1)
        assertEquals(dbHelper.getGarmentDao().create(dbModelHelper.testGarment2), 1)
        GetGarmentsInteractor(dbHelper)
                .execute()
                .test()
                .assertSubscribed()
                .assertResult(GetGarmentsInteractor.Result(listOf(dbModelHelper.testGarment2), listOf(dbModelHelper.testGarment1)))
                //.assertValue(GetGarmentsInteractor.Result(listOf(dbModelHelper.testGarment2), listOf(dbModelHelper.testGarment1)))
                .assertNoErrors()

    }
}