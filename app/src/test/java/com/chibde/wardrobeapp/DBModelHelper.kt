package com.chibde.wardrobeapp

import com.chibde.wardrobeapp.model.Garment
import com.chibde.wardrobeapp.model.GarmentType

/**
 * Created by gautam on 28/6/18.
 */
class DBModelHelper {
    val testGarment1 = Garment(type = GarmentType.PANT.name, url = "testUrl1")
    val testGarment2 = Garment(type = GarmentType.SHIRT.name, url = "testUrl2")
}