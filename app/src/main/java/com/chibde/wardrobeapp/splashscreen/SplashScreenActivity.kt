package com.chibde.wardrobeapp.splashscreen

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import com.chibde.wardrobeapp.R
import com.chibde.wardrobeapp.main.MainActivity
import com.chibde.wardrobeapp.utils.Utils
import io.reactivex.Observable
import org.jetbrains.anko.alert
import java.util.concurrent.TimeUnit

class SplashScreenActivity : AppCompatActivity() {
    private fun getWriteExternalStoragePerms(): Array<String> {
        return arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        requestPermissionsForApp()
    }

    private fun requestPermissionsForApp() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, getWriteExternalStoragePerms(), EXTERNAL_STORAGE_REQUEST)
        } else {
            startMainActivity()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            EXTERNAL_STORAGE_REQUEST -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startMainActivity()
            } else {
                alert {
                    title = "Warnning"
                    message = "permission are required"
                    positiveButton("ok") { finish() }
                }.show()
            }
        }
    }

    private fun startMainActivity() {
        Observable.just(1)
                .delay(2, TimeUnit.SECONDS)
                .subscribe({
                    runOnUiThread {
                        startActivity(Intent(this, MainActivity::class.java))
                        finish()
                    }
                }, { error -> Utils.loge(javaClass.simpleName, error.message) })
    }

    companion object {
        const val EXTERNAL_STORAGE_REQUEST = 102
    }
}