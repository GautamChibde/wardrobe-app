package com.chibde.wardrobeapp.utils

import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import com.chibde.wardrobeapp.BuildConfig
import java.text.SimpleDateFormat
import java.util.*

object Utils {

    fun getRealPathFromURI(context: Context, contentUri: Uri): String {
        var cursor: Cursor? = null
        try {
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            cursor = context.contentResolver.query(contentUri, proj, null, null, null)
            val columnIndex = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            return cursor.getString(columnIndex)
        } finally {
            if (cursor != null) {
                cursor.close()
            }
        }
    }

    fun getFormattedDate(milliSeconds: Long): String {
        val formatter = SimpleDateFormat("EEE d MMM yyyy", Locale.US)
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSeconds
        return formatter.format(calendar.time)
    }

    fun loge(simpleName: String, e: String?) {
        if (BuildConfig.DEBUG) {
            Log.e(simpleName, "error $e")
        }
    }
}