package com.chibde.wardrobeapp.utils

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity
import com.chibde.wardrobeapp.BuildConfig
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class ImageUtils(private val context: Context) {
    var savedImagePath: String? = null

    fun cameraIntent(requestCode: Int) {
        val photoPath = createImageFile()
        savedImagePath = photoPath.absolutePath
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        var uri = Uri.fromFile(photoPath)
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", photoPath)
        }
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
        (context as AppCompatActivity).startActivityForResult(takePictureIntent, requestCode)
    }

    private fun createImageFile(): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)

        // Save a file: path for use with ACTION_VIEW intents
        return File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir      /* directory */
        )
    }
}