package com.chibde.wardrobeapp.utils

import android.widget.ImageView
import com.chibde.wardrobeapp.R
import com.squareup.picasso.Picasso
import java.io.File
import java.util.*

fun ClosedRange<Int>.random() = Random().nextInt(endInclusive - start) + start

fun ImageView.loadImagePicasso(url: String) {
    Picasso.get()
            .load(File(url))
            .resize(720, 720)
            .error(R.drawable.error)
            .placeholder(R.drawable.placeholder)
            .into(this)
}