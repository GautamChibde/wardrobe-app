package com.chibde.wardrobeapp.main

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.chibde.wardrobeapp.R
import com.chibde.wardrobeapp.favourite.FavouriteActivity
import com.chibde.wardrobeapp.main.MainActivityPresenter.Companion.PICK_IMAGE_CAMERA
import com.chibde.wardrobeapp.main.MainActivityPresenter.Companion.PICK_IMAGE_GALLERY
import com.chibde.wardrobeapp.main.adapter.ImagePagerAdapter
import com.chibde.wardrobeapp.model.Garment
import com.chibde.wardrobeapp.utils.ImageUtils
import com.chibde.wardrobeapp.utils.Utils
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar_main_activity.*
import org.jetbrains.anko.selector
import org.jetbrains.anko.toast

class MainActivity : AppCompatActivity(), MainActivityView {

    private val presenter: MainActivityPresenter by lazy { MainActivityPresenter(this) }
    private val imageUtils: ImageUtils by lazy { ImageUtils(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initPager()
        initToolbar()
        initListeners()

        savedInstanceState?.let {
            loadPantData(savedInstanceState.getParcelableArrayList(PANTS_LIST))
            loadShirtData(savedInstanceState.getParcelableArrayList(SHIRT_LIST))
            activity_main_view_pager_pants.currentItem = savedInstanceState.getInt(PANT_LIST_POSITION)
            activity_main_view_pager_shirts.currentItem = savedInstanceState.getInt(SHIRT_LIST_POSITION)
            presenter.listEmptyScreen()
            presenter.checkFavourited()
        } ?: run {
            presenter.onInit()
        }
    }

    private fun initPager() {
        activity_main_view_pager_pants.adapter = ImagePagerAdapter(this, ArrayList())
        activity_main_view_pager_shirts.adapter = ImagePagerAdapter(this, ArrayList())
    }

    private fun initToolbar() {
        toolbar_main_activity_tv_title.text = getString(R.string.app_name)
        toolbar_main_activity_ib_favorite.setOnClickListener {
            startFavouriteActivity()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList(PANTS_LIST, getPantsList())
        outState.putParcelableArrayList(SHIRT_LIST, getShirtList())
        outState.putInt(PANT_LIST_POSITION, activity_main_view_pager_pants.currentItem)
        outState.putInt(SHIRT_LIST_POSITION, activity_main_view_pager_shirts.currentItem)
    }

    private fun startFavouriteActivity() {
        startActivity(Intent(this, FavouriteActivity::class.java))
    }

    private fun initListeners() {
        activity_main_btn_add_pant.setOnClickListener { presenter.onAddPantButtonClicked() }
        activity_main_btn_add_shirts.setOnClickListener { presenter.onAddShirtButtonClicked() }
        activity_main_btn_random.setOnClickListener { presenter.onSelectRandom() }
        activity_main_btn_favorite.setOnClickListener { presenter.onFavoriteClicked() }

        activity_main_view_pager_pants.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                presenter.checkFavourited()
            }
        })

        activity_main_view_pager_shirts.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                presenter.checkFavourited()
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun loadPantData(pants: List<Garment>) {
        (activity_main_view_pager_pants.adapter as ImagePagerAdapter).setItems(pants)
    }

    override fun onError(message: String?) {
        toast(message ?: "")
    }

    override fun loadShirtData(shirts: List<Garment>) {
        (activity_main_view_pager_shirts.adapter as ImagePagerAdapter).setItems(shirts)
    }

    override fun setRandom(pantPos: Int, shirtPos: Int) {
        activity_main_view_pager_pants.currentItem = pantPos
        activity_main_view_pager_shirts.currentItem = shirtPos
    }

    override fun addShirt(shirt: Garment) {
        (activity_main_view_pager_shirts.adapter as ImagePagerAdapter).addItem(shirt)
        activity_main_view_pager_shirts.currentItem = getShirtsListSize()
    }

    override fun addPant(pant: Garment) {
        (activity_main_view_pager_pants.adapter as ImagePagerAdapter).addItem(pant)
        activity_main_view_pager_pants.currentItem = getPantsListSize()
    }

    override fun setFavourited() {
        activity_main_btn_favorite.setColorFilter(Color.BLACK)
    }

    override fun setUnFavourited() {
        activity_main_btn_favorite.setColorFilter(Color.WHITE)
    }

    override fun showImageSelectionDialog(role: Int) {
        val options = listOf("Take Photo", "Choose From Gallery", "Cancel")
        selector("Select Option", options, { dialogInterface, i ->
            when {
                options[i] == "Take Photo" -> {
                    imageUtils.cameraIntent(PICK_IMAGE_CAMERA + role)
                }
                options[i] == "Choose From Gallery" -> {
                    val pickPhoto = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    intent.type = MIME_TYPE_IMAGE
                    startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY + role)
                }
            }
            dialogInterface.dismiss()
        })
    }

    override fun showShirtEmptyScreen() {
        activity_main_tv_no_shirts.visibility = View.VISIBLE
    }

    override fun showPantsEmptyScreen() {
        activity_main_tv_no_pants.visibility = View.VISIBLE
    }

    override fun hidePantsEmptyScreen() {
        activity_main_tv_no_pants.visibility = View.GONE
    }

    override fun hideShirtEmptyScreen() {
        activity_main_tv_no_shirts.visibility = View.GONE
    }

    override fun onRestart() {
        super.onRestart()
        presenter.onInit()
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        data?.let {
            data.data?.let {
                presenter.onActivityResultGallery(requestCode, Utils.getRealPathFromURI(this, data.data))
            }
        } ?: run {
            presenter.onActivityResultCamera(requestCode, imageUtils.savedImagePath, resultCode)
        }
    }

    @Throws(IndexOutOfBoundsException::class)
    override fun getSelectedShirt(): Garment {
        return (activity_main_view_pager_shirts.adapter as ImagePagerAdapter).getItems()[activity_main_view_pager_shirts.currentItem]
    }

    @Throws(IndexOutOfBoundsException::class)
    override fun getSelectedPant(): Garment {
        return (activity_main_view_pager_pants.adapter as ImagePagerAdapter).getItems()[activity_main_view_pager_pants.currentItem]
    }

    override fun showFavouriteButton() {
        activity_main_btn_favorite.visibility = View.VISIBLE
    }

    override fun hideFavouriteButton() {
        activity_main_btn_favorite.visibility = View.GONE
    }

    override fun hideShuffleButton() {
        activity_main_btn_random.visibility = View.GONE
    }

    override fun showShuffleButton() {
        activity_main_btn_random.visibility = View.VISIBLE
    }

    override fun getShirtsListSize(): Int = (activity_main_view_pager_shirts.adapter as ImagePagerAdapter).count

    override fun getPantsListSize(): Int = (activity_main_view_pager_pants.adapter as ImagePagerAdapter).count

    private fun getPantsList(): ArrayList<Garment> = (activity_main_view_pager_pants.adapter as ImagePagerAdapter).getItems()

    private fun getShirtList(): ArrayList<Garment> = (activity_main_view_pager_shirts.adapter as ImagePagerAdapter).getItems()

    companion object {
        const val MIME_TYPE_IMAGE = "image/*"
        const val PANTS_LIST = "pants"
        const val SHIRT_LIST = "shirt"
        const val PANT_LIST_POSITION = "pants_position"
        const val SHIRT_LIST_POSITION = "shirt_position"
    }
}
