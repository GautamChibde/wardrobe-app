package com.chibde.wardrobeapp.main.interactor

import com.chibde.wardrobeapp.db.utils.FavouriteTableUtils
import com.chibde.wardrobeapp.model.Favourite
import com.chibde.wardrobeapp.model.Garment
import com.chibde.wardrobeapp.utils.Utils
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class FavouriteItemInteractor {
    fun execute(shirt: Garment, pant: Garment): Single<Result> {
        return Single.fromCallable {
            if (FavouriteTableUtils.isFavorited(shirt, pant)) {
                FavouriteTableUtils.removeFavourite(shirt, pant)
                Result(true, null)
            } else {
                Result(false, FavouriteTableUtils.createFavorite(shirt, pant))
            }
        }
                .doOnError { Utils.loge(javaClass.simpleName, "error $it.message") }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    data class Result(var deleted: Boolean, var favourite: Favourite?)
}