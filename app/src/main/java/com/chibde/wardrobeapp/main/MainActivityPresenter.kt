package com.chibde.wardrobeapp.main

import android.app.Activity
import com.chibde.wardrobeapp.db.DatabaseHelper
import com.chibde.wardrobeapp.main.interactor.CheckIfFavouritedInteractor
import com.chibde.wardrobeapp.main.interactor.FavouriteItemInteractor
import com.chibde.wardrobeapp.main.interactor.GetGarmentsInteractor
import com.chibde.wardrobeapp.main.interactor.SaveGarmentInteractor
import com.chibde.wardrobeapp.model.Garment
import com.chibde.wardrobeapp.model.GarmentType
import com.chibde.wardrobeapp.utils.random
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo

class MainActivityPresenter(private val view: MainActivityView) {

    private val disposables = CompositeDisposable()

    fun onInit() {
        loadPagerData()
    }

    private fun loadPagerData() {
        GetGarmentsInteractor(DatabaseHelper.getInstance()).execute()
                .subscribe({ result ->
                    view.loadPantData(result.pants)
                    view.loadShirtData(result.shirts)
                    checkFavourited()
                    listEmptyScreen()
                }, { error -> view.onError(error.message) })
                .addTo(disposables)
    }


    fun onDestroy() {
        disposables.dispose()
    }

    fun onAddPantButtonClicked() {
        view.showImageSelectionDialog(PANT_SELECTION)
    }

    fun onAddShirtButtonClicked() {
        view.showImageSelectionDialog(SHIRT_SELECTION)
    }

    fun onSelectRandom() {
        if (isListNotEmpty()) {
            view.setRandom(
                    (0..view.getPantsListSize()).random(),
                    (0..view.getShirtsListSize()).random()
            )
        }
    }

    fun onActivityResultGallery(requestCode: Int, path: String?) {
        path?.let {
            val garmentToSave = Garment()
            garmentToSave.url = path
            when (requestCode) {
                PICK_IMAGE_GALLERY + PANT_SELECTION -> {
                    garmentToSave.type = GarmentType.PANT.name
                }
                PICK_IMAGE_GALLERY + SHIRT_SELECTION -> {
                    garmentToSave.type = GarmentType.SHIRT.name
                }
            }
            saveGarment(garmentToSave)
        }
    }

    private fun saveGarment(garmentToSave: Garment): Disposable {
        return SaveGarmentInteractor().execute(garmentToSave)
                .subscribe(
                        { garment ->
                            garment?.let {
                                when (garment.type) {
                                    GarmentType.PANT.name -> view.addPant(garment)
                                    GarmentType.SHIRT.name -> view.addShirt(garment)
                                    else -> { // Nothing to do here
                                    }
                                }
                                listEmptyScreen()
                                checkFavourited()
                            } ?: run {
                                view.onError("Error adding item")
                            }
                        }
                ).addTo(disposables)
    }

    fun onActivityResultCamera(requestCode: Int, savedImagePath: String?, result: Int) {
        val garmentToSave = Garment()
        savedImagePath?.let {
            if (result == Activity.RESULT_OK) {
                when (requestCode) {
                    PICK_IMAGE_CAMERA + PANT_SELECTION -> {
                        garmentToSave.type = GarmentType.PANT.name
                        garmentToSave.url = savedImagePath
                    }
                    PICK_IMAGE_CAMERA + SHIRT_SELECTION -> {
                        garmentToSave.type = GarmentType.SHIRT.name
                        garmentToSave.url = savedImagePath
                    }
                }
                saveGarment(garmentToSave)
            }
        }
    }

    fun onFavoriteClicked() {
        if (isListNotEmpty()) {
            FavouriteItemInteractor().execute(shirt = view.getSelectedShirt(),
                    pant = view.getSelectedPant())
                    .subscribe({ result ->
                        if (result.deleted) {
                            view.setUnFavourited()
                        } else {
                            result.favourite?.let {
                                view.setFavourited()
                            } ?: run {
                                view.onError("")
                            }
                        }
                    }, { error ->
                        view.onError(error.message)
                    })
                    .addTo(disposables)
        }
    }

    private fun isListNotEmpty(): Boolean = view.getPantsListSize() != 0 && view.getShirtsListSize() != 0

    private fun isAnyListEmpty(): Boolean = view.getPantsListSize() == 0 || view.getShirtsListSize() == 0

    fun checkFavourited() {
        if (isListNotEmpty()) {
            CheckIfFavouritedInteractor().execute(
                    shirt = view.getSelectedShirt(),
                    pant = view.getSelectedPant()
            ).subscribe({ t -> if (t) view.setFavourited() else view.setUnFavourited() },
                    { error -> view.onError(error.message) }
            ).addTo(disposables)
        }
    }

    fun listEmptyScreen() {
        if (view.getPantsListSize() == 0) view.showPantsEmptyScreen() else view.hidePantsEmptyScreen()
        if (view.getShirtsListSize() == 0) view.showShirtEmptyScreen() else view.hideShirtEmptyScreen()
        if (isAnyListEmpty()) {
            view.hideFavouriteButton()
            view.hideShuffleButton()
        } else {
            view.showFavouriteButton()
            if (view.getShirtsListSize() == 1 && view.getPantsListSize() == 1) {
                view.hideShuffleButton()
            } else {
                view.showShuffleButton()
            }
        }
    }

    companion object {
        const val PANT_SELECTION = 10
        const val SHIRT_SELECTION = 13
        const val PICK_IMAGE_CAMERA = 1
        const val PICK_IMAGE_GALLERY = 2
    }
}