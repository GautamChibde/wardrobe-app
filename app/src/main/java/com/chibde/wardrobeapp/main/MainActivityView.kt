package com.chibde.wardrobeapp.main

import com.chibde.wardrobeapp.model.Garment

interface MainActivityView {
    fun loadPantData(pants: List<Garment>)
    fun onError(message: String?)
    fun loadShirtData(shirts: List<Garment>)
    fun getShirtsListSize(): Int
    fun getPantsListSize(): Int
    fun setRandom(pantPos: Int, shirtPos: Int)
    fun showImageSelectionDialog(role: Int)
    fun addPant(pant: Garment)
    fun addShirt(shirt: Garment)
    fun getSelectedShirt() : Garment
    fun getSelectedPant() : Garment
    fun setFavourited()
    fun setUnFavourited()
    fun showPantsEmptyScreen()
    fun showShirtEmptyScreen()
    fun hidePantsEmptyScreen()
    fun hideShirtEmptyScreen()
    fun showFavouriteButton()
    fun hideFavouriteButton()
    fun hideShuffleButton()
    fun showShuffleButton()
}