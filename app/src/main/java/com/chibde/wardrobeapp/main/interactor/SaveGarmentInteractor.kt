package com.chibde.wardrobeapp.main.interactor

import com.chibde.wardrobeapp.db.utils.GarmentTableUtils
import com.chibde.wardrobeapp.model.Garment
import com.chibde.wardrobeapp.utils.Utils
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SaveGarmentInteractor {

    fun execute(garment: Garment): Single<Garment?> {
        return Single
                .fromCallable {
                    GarmentTableUtils.createGarment(garment)
                }
                .doOnError { Utils.loge(javaClass.simpleName, "error $it.message") }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}