package com.chibde.wardrobeapp.main.interactor

import com.chibde.wardrobeapp.db.utils.FavouriteTableUtils
import com.chibde.wardrobeapp.model.Garment
import com.chibde.wardrobeapp.utils.Utils
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class CheckIfFavouritedInteractor {

    fun execute(shirt: Garment, pant: Garment): Single<Boolean> {
        return Single.fromCallable { FavouriteTableUtils.isFavorited(shirt, pant) }
                .doOnError { Utils.loge(javaClass.simpleName, "error $it.message") }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}