package com.chibde.wardrobeapp.main.interactor

import com.chibde.wardrobeapp.db.DatabaseHelper
import com.chibde.wardrobeapp.db.utils.GarmentTableUtils
import com.chibde.wardrobeapp.model.Garment
import com.chibde.wardrobeapp.model.GarmentType
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers

class GetGarmentsInteractor(private val databaseHelper: DatabaseHelper) {

    fun execute(): Observable<Result> {
        return Observable.zip(
                Observable.fromCallable { GarmentTableUtils.getGarmentByType(databaseHelper, GarmentType.SHIRT.name) }.subscribeOn(Schedulers.newThread()),
                Observable.fromCallable { GarmentTableUtils.getGarmentByType(databaseHelper, GarmentType.PANT.name) }.subscribeOn(Schedulers.newThread()),
                BiFunction { shirts: List<Garment>, pants: List<Garment> -> Result(shirts, pants) })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    data class Result(var shirts: List<Garment>, var pants: List<Garment>)
}