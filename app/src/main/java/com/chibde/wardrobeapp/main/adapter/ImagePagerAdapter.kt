package com.chibde.wardrobeapp.main.adapter

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.chibde.wardrobeapp.model.Garment
import com.chibde.wardrobeapp.utils.loadImagePicasso

class ImagePagerAdapter(private var context: Context,
                        private var items: MutableList<Garment>) : PagerAdapter() {
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imageView = ImageView(context)
        imageView.scaleType = ImageView.ScaleType.CENTER_CROP
        imageView.loadImagePicasso(items[position].url)
        container.addView(imageView)
        return imageView
    }

    override fun getCount(): Int = items.size

    override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object`

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (container as ViewPager).removeView(`object` as View)
    }

    fun addItem(garment: Garment) {
        items.add(garment)
        notifyDataSetChanged()
    }

    fun setItems(garments : List<Garment>) {
        items.clear()
        items.addAll(garments)
        notifyDataSetChanged()
    }

    fun getItems(): ArrayList<Garment> {
        return ArrayList(items)
    }
}