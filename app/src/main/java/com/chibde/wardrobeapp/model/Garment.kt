package com.chibde.wardrobeapp.model

import android.os.Parcel
import android.os.Parcelable
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable

@DatabaseTable
data class Garment(@DatabaseField(generatedId = true) var id: Int = 0,
                   @DatabaseField var type: String = GarmentType.NONE.name.toLowerCase(),
                   @DatabaseField var url: String = "") : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(type)
        parcel.writeString(url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Garment> {
        override fun createFromParcel(parcel: Parcel): Garment {
            return Garment(parcel)
        }

        override fun newArray(size: Int): Array<Garment?> {
            return arrayOfNulls(size)
        }
    }
}

enum class GarmentType {
    SHIRT, PANT, NONE,
}

const val GARMENT_TYPE = "type"