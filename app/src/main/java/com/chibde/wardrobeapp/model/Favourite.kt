package com.chibde.wardrobeapp.model

import android.os.Parcel
import android.os.Parcelable
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import java.util.*

@DatabaseTable
data class Favourite(@DatabaseField(generatedId = true) var id: Int = 0,
                     @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true, columnName = FAVOURITE_SHIRT_ID, foreignColumnName = "id")
                     var shirt: Garment = Garment(),
                     @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true, columnName = FAVOURITE_PANT_ID, foreignColumnName = "id")
                     var pant: Garment = Garment(),
                     @DatabaseField()
                     var timeStamp: Long = Calendar.getInstance().timeInMillis) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readParcelable(Garment::class.java.classLoader),
            parcel.readParcelable(Garment::class.java.classLoader),
            parcel.readLong())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeParcelable(shirt, flags)
        parcel.writeParcelable(pant, flags)
        parcel.writeLong(timeStamp)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Favourite> {
        override fun createFromParcel(parcel: Parcel): Favourite {
            return Favourite(parcel)
        }

        override fun newArray(size: Int): Array<Favourite?> {
            return arrayOfNulls(size)
        }
    }
}

const val FAVOURITE_SHIRT_ID = "shirt_id"
const val FAVOURITE_PANT_ID = "pant_id"