package com.chibde.wardrobeapp.favourite.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chibde.wardrobeapp.R
import com.chibde.wardrobeapp.model.Favourite
import com.chibde.wardrobeapp.utils.Utils
import com.chibde.wardrobeapp.utils.loadImagePicasso
import kotlinx.android.synthetic.main.item_favourite.view.*

class FavouriteAdapter(private val onFavouriteRemoved: (Favourite) -> Unit,
                       private var items: MutableList<Favourite>) : RecyclerView.Adapter<FavouriteAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_favourite, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder.itemView) {
            item_favourite_image_shirt.loadImagePicasso(items[position].pant.url)
            item_favourite_image_pant.loadImagePicasso(items[position].shirt.url)
            item_favourite_ib_delete.setOnClickListener {
                onFavouriteRemoved(items[position])
            }
            item_favourite_tv_timestamp.text = Utils.getFormattedDate(items[position].timeStamp)
        }
    }

    fun removeItem(favourite: Favourite) {
        val position = items.indexOf(favourite)
        items.remove(favourite)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, items.size)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}