package com.chibde.wardrobeapp.favourite.interactor

import com.chibde.wardrobeapp.db.utils.FavouriteTableUtils
import com.chibde.wardrobeapp.model.Favourite
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GetFavouriteInteractor {

    fun execute() : Observable<List<Favourite>> {
        return Observable.fromCallable { FavouriteTableUtils.getAllFavourites() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}