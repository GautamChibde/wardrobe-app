package com.chibde.wardrobeapp.favourite

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.chibde.wardrobeapp.R
import com.chibde.wardrobeapp.favourite.adapter.FavouriteAdapter
import com.chibde.wardrobeapp.model.Favourite
import kotlinx.android.synthetic.main.activity_favourite.*
import kotlinx.android.synthetic.main.toolbar_favourite_activity.*
import org.jetbrains.anko.toast

class FavouriteActivity : AppCompatActivity(), FavouriteActivityView {
    private val presenter: FavouriteActivityPresenter by lazy { FavouriteActivityPresenter(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favourite)
        initToolbar()
        presenter.onInit()
    }

    private fun initToolbar() {
        toolbar_activity_favourite_ib_back.setOnClickListener { finish() }
        toolbar_tv_title.text = getString(R.string.favourite_activity_titles)
    }

    override fun onLoadFavourite(favourites: List<Favourite>) {
        activity_favourite_rv.layoutManager = LinearLayoutManager(this)
        activity_favourite_rv.adapter = FavouriteAdapter(presenter::onFavouriteRemoved, favourites.toMutableList())
    }

    override fun onError(message: String?) {
        toast(message ?: "")
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun removeFavourite(favourite: Favourite) {
        (activity_favourite_rv.adapter as FavouriteAdapter).removeItem(favourite)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }
}
