package com.chibde.wardrobeapp.favourite

import com.chibde.wardrobeapp.model.Favourite

interface FavouriteActivityView {
    fun onLoadFavourite(favourites: List<Favourite>)
    fun onError(message: String?)
    fun removeFavourite(favourite: Favourite)
}