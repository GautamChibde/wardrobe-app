package com.chibde.wardrobeapp.favourite

import com.chibde.wardrobeapp.favourite.interactor.DeleteFavoriteInteractor
import com.chibde.wardrobeapp.favourite.interactor.GetFavouriteInteractor
import com.chibde.wardrobeapp.model.Favourite
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo

class FavouriteActivityPresenter(private val view: FavouriteActivityView) {
    private val disposables = CompositeDisposable()

    fun onInit() {
        GetFavouriteInteractor().execute()
                .subscribe({ favourites ->
                    view.onLoadFavourite(favourites)
                }, { error -> view.onError(error.message) })
                .addTo(disposables)
    }

    fun onFavouriteRemoved(favourite: Favourite) {
        DeleteFavoriteInteractor().execute(favourite)
                .subscribe( {
                    t -> if(t) view.removeFavourite(favourite)
                })
                .addTo(disposables)
    }

    fun onDestroy() {
        disposables.dispose()
    }
}