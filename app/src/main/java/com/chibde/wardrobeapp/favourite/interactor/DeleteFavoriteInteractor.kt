package com.chibde.wardrobeapp.favourite.interactor

import com.chibde.wardrobeapp.db.utils.FavouriteTableUtils
import com.chibde.wardrobeapp.model.Favourite
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by gautam on 5/6/18.
 */
class DeleteFavoriteInteractor {
    fun execute(favourite: Favourite) : Single<Boolean> {
        return Single.fromCallable { FavouriteTableUtils.removeFavourite(favourite )}
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}