package com.chibde.wardrobeapp

import android.app.Application
import com.chibde.wardrobeapp.db.DatabaseHelper

class WardrobeApp : Application() {

    override fun onCreate() {
        super.onCreate()
        DatabaseHelper.init(applicationContext)
    }
}
