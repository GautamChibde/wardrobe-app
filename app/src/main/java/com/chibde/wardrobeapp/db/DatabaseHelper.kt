package com.chibde.wardrobeapp.db

import android.content.Context
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import com.chibde.wardrobeapp.model.Favourite
import com.chibde.wardrobeapp.model.Garment
import com.chibde.wardrobeapp.utils.Utils
import com.j256.ormlite.android.apptools.OpenHelperManager
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableUtils

class DatabaseHelper(context: Context)
    : OrmLiteSqliteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    override fun onCreate(database: SQLiteDatabase?, connectionSource: ConnectionSource?) {
        try {
            TableUtils.createTable(connectionSource, Garment::class.java)
            TableUtils.createTable(connectionSource, Favourite::class.java)
        } catch (e: SQLException) {
            Utils.loge(DatabaseHelper::class.java.simpleName, "onCreate $e")
        }
    }

    override fun onUpgrade(database: SQLiteDatabase?, connectionSource: ConnectionSource?, oldVersion: Int, newVersion: Int) {
        if (oldVersion < 2) {
            upgradeTo2()
        }
    }

    private fun upgradeTo2() {
        try {
            TableUtils.createTable(connectionSource, Favourite::class.java)
        } catch (e: SQLException) {
            Utils.loge(DatabaseHelper::class.java.simpleName, "onCreate $e")
        }
    }

    @Throws(SQLException::class)
    fun getGarmentDao(): Dao<Garment, Int> {
        return getDao(Garment::class.java)
    }

    @Throws(SQLException::class)
    fun getFavouriteDao(): Dao<Favourite, Int> {
        return getDao(Favourite::class.java)
    }

    companion object {
        const val DATABASE_NAME = "wardrobe_app.db"
        const val DATABASE_VERSION = 2

        private var INSTANCE: DatabaseHelper? = null

        fun getInstance(): DatabaseHelper {
            return INSTANCE!!
        }

        fun init(context: Context) {
            if (INSTANCE == null) {
                INSTANCE = OpenHelperManager.getHelper(context, DatabaseHelper::class.java)
            }
        }
    }
}