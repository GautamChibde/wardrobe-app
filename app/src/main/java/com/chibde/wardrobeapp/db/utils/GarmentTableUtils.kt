package com.chibde.wardrobeapp.db.utils

import com.chibde.wardrobeapp.db.DatabaseHelper
import com.chibde.wardrobeapp.model.GARMENT_TYPE
import com.chibde.wardrobeapp.model.Garment
import com.chibde.wardrobeapp.utils.Utils
import com.j256.ormlite.dao.Dao

object GarmentTableUtils {

    fun getGarmentByType(databaseHelper: DatabaseHelper, type: String): List<Garment> {
        val garments = ArrayList<Garment>()
        try {
            val garmentDao: Dao<Garment, Int> = databaseHelper.getGarmentDao()
            garmentDao.queryBuilder().where().eq(GARMENT_TYPE, type).query()?.let {
                garments.addAll(it)
            }
        } catch (e: Exception) {
            Utils.loge(GarmentTableUtils::class.java.simpleName, "getGarmentByType $e")
        }
        return garments
    }

    fun createGarment(garment: Garment): Garment? {
        try {
            val garmentDao: Dao<Garment, Int> = DatabaseHelper.getInstance().getGarmentDao()
            val g = garmentDao.createIfNotExists(garment)
            return g
        } catch (e: Exception) {
            Utils.loge(GarmentTableUtils::class.java.simpleName, "createGarment $e")
        }
        return null
    }
}