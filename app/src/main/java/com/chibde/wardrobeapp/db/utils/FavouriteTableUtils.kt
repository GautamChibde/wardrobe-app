package com.chibde.wardrobeapp.db.utils

import com.chibde.wardrobeapp.db.DatabaseHelper
import com.chibde.wardrobeapp.model.FAVOURITE_PANT_ID
import com.chibde.wardrobeapp.model.FAVOURITE_SHIRT_ID
import com.chibde.wardrobeapp.model.Favourite
import com.chibde.wardrobeapp.model.Garment
import com.chibde.wardrobeapp.utils.Utils
import com.j256.ormlite.dao.Dao

object FavouriteTableUtils {

    fun createFavorite(shirt: Garment, pant: Garment): Favourite? {
        val favourite = Favourite(shirt = shirt, pant = pant)
        try {
            val favouriteDao: Dao<Favourite, Int> = DatabaseHelper.getInstance().getFavouriteDao()
            return favouriteDao.createIfNotExists(favourite)
        } catch (e: Exception) {
            Utils.loge(GarmentTableUtils::class.java.simpleName, "createFavorite $e")
        }
        return null
    }

    fun getFavourite(shirt: Garment, pant: Garment): Favourite? {
        var favourite: Favourite? = null
        try {
            val favouriteDao: Dao<Favourite, Int> = DatabaseHelper.getInstance().getFavouriteDao()
            favouriteDao.queryBuilder().where()
                    .eq(FAVOURITE_SHIRT_ID, shirt.id)
                    .and()
                    .eq(FAVOURITE_PANT_ID, pant.id)
                    .queryForFirst()?.let {
                        favourite = it
                    }
        } catch (e: Exception) {
            Utils.loge(GarmentTableUtils::class.java.simpleName, "getGarmentByType $e")
        }
        return favourite
    }

    fun isFavorited(shirt: Garment, pant: Garment) : Boolean {
        return getFavourite(shirt, pant) != null
    }

    fun getAllFavourites(): List<Favourite> {
        val favourites = ArrayList<Favourite>()
        try {
            val favouriteDao: Dao<Favourite, Int> = DatabaseHelper.getInstance().getFavouriteDao()
            favouriteDao.queryForAll()?.let {
                favourites.addAll(it)
            }
        } catch (e: Exception) {
            Utils.loge(GarmentTableUtils::class.java.simpleName, "getGarmentByType $e")
        }
        return favourites
    }

    fun removeFavourite(favourite: Favourite) : Boolean {
        return try {
            val favouriteDao: Dao<Favourite, Int> = DatabaseHelper.getInstance().getFavouriteDao()
            favouriteDao.delete(favourite) >= 1
        } catch (e: Exception) {
            Utils.loge(GarmentTableUtils::class.java.simpleName, "getGarmentByType $e")
            false
        }
    }

    fun removeFavourite(shirt: Garment, pant: Garment) {
        try {
            val favouriteDao: Dao<Favourite, Int> = DatabaseHelper.getInstance().getFavouriteDao()
            val deleteBuilder = favouriteDao.deleteBuilder()
            deleteBuilder.where()
                    .eq(FAVOURITE_SHIRT_ID, shirt.id)
                    .and()
                    .eq(FAVOURITE_PANT_ID, pant.id)
            deleteBuilder.delete()
        } catch (e: Exception) {
            Utils.loge(GarmentTableUtils::class.java.simpleName, "getGarmentByType $e")
        }
    }
}